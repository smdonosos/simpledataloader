import sys, abc, pyodbc, traceback, pandas as pd
from retrying import retry
from logger import LOG, LoggingHandler

#Glabal variables

LoggingHandler()

class DataSource(metaclass=abc.ABCMeta):
    ''' Metaclass to define DataSource absctract class '''

    @abc.abstractmethod
    def _get_data_frame(self):
        ''' Execute qry and get a data '''
        raise NotImplementedError

    @abc.abstractmethod
    def _run_sql_statement(self):
        ''' Execute sql statement (for inserst) '''
        raise NotImplementedError

    @abc.abstractmethod
    def _run_vertica_copy(self):
        ''' Execute vertica copy statement '''
        raise NotImplementedError
    
class DataBaseSource(DataSource):
    attemps = None
    timeout = None
    delay = None
    
    def __init__(self, connection, qry_string, opt_param=None):

        self._connection = connection
        self._qry_string = qry_string
        self._opt_param = opt_param

    @retry(stop_max_attempt_number=attemps, wait_fixed=delay)
    def _get_data_frame(self):
        """ Execute query, returns a dataframe """
        #LOG.debug(self._qry_string)
        #data_chunk = []
        try:
            odbc_connection = pyodbc.connect(self._connection)
            df = pd.read_sql(self._qry_string, odbc_connection)
            """
            for chunk in pd.read_sql(self._qry_string, odbc_connection, chunksize=10000):
                data_chunk.append(chunk)
            odbc_connection.close()
            
            if len(data_chunk) > 1:
                df = pd.concat(data_chunk, axis=0)
                df = df.reset_index(drop=True)
            elif len(data_chunk) == 1:
                df = data_chunk[0]
            else:
                df = pd.DataFrame()
            """
            return df
        except:
            traceback.print_exc()
            sys.exit(-1)

    @retry(stop_max_attempt_number=attemps, wait_fixed=delay)
    def _run_sql_statement(self):
        '''  '''
        qry_1 = f"INSERT INTO {self._opt_param['SCHEMA']}.{self._opt_param['TABLE']}"
        qry_2 = f"({self._opt_param['COLUMNS']}) \n"
        qry_3 = f"VALUES ({self._opt_param['COLVALS']})"
        qry = qry_1 + qry_2 + qry_3

        odbc_connection = pyodbc.connect(self._connection)
        odbc_connection.autocommit = False
        cursor = odbc_connection.cursor()
        # cursor.fast_executemany = True
        cursor.executemany(qry, self._qry_string)
        odbc_connection.commit()
        odbc_connection.close()

    @retry(stop_max_attempt_number=attemps, wait_fixed=delay)
    def _run_vertica_copy(self):
        ''' Run special vertica Copy
        To load data to tables thorugh odbc
        ------------------------
        '''
        odbc_connection = pyodbc.connect(self._connection)
        cursor = odbc_connection.cursor()
        cursor.execute(self._qry_string)
        odbc_connection.close()
        
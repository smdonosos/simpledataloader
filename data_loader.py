from calendar import datetime
import argparse, sys, traceback
from custom_logics import types, exceptions as ce, validation as val
from logger import LOG, LoggingHandler
from loader import Loader

LoggingHandler()

def main():
    
    time1 = datetime.datetime.now()
    LOG.warning(f"Starting process at {time1}")
    LOG.debug("Parsing arguments...")
    parser_description = """
                        Argument parser for Simple Data Loader
                        """
    main_parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter
                , description=parser_description)
    main_parser.add_argument("engine", type=str, help="DBEngine to use, must be configured on dbengines.json")
    main_parser.add_argument("filter_parameters", type=types._dic_type, help="Parameters dictionary in format 'PARAMETER-n':VALUE-n")
    argument = main_parser.parse_args()

    LOG.debug("Validating arguments...")
    try:
        validation_result = vars(val.ArgumentValidation(argument.engine, argument.filter_parameters))["_val_res"]
        Loader(validation_result)
        time2 = datetime.datetime.now()
        #tdelta = datetime.timedelta(time2-time1).seconds()
        LOG.warning(f"Process ended at {time2}")
        #LOG.warning(f"Process took {tdelta} seconds to run")
    except ce.EngineError as error:    
        LOG.critical(f"Engine {error.msg} not found in dbengines.json, check the parameters and try again")
        sys.exit(-1)
    except ce.KeyFiltersError:
        LOG.critical(f"Incoming parameters do not match with engine configuration")
        sys.exit(-1)
    except ce.IncomingParameterCombinationError:
        LOG.critical(f"Incoming parameters combination do not match with any parameter combination configured on the engine")
        sys.exit(-1)
    except ce.IncomingParameterLengthError as error:
        LOG.critical(f"The length of parameters {error.msg} do not match with engine configuration")
    except Exception:
        LOG.critical(traceback.format_exc())
        sys.exit(-2)
        
if __name__ == "__main__":
    main()
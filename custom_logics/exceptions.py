class Error(Exception):
    """  Base class for custom exceptions """

class EngineError(Error):
    ''' Raised when the engine provided in the command line
    do not match with any of the engines configured in the
    dbengines.json file.
    -----
    '''
    def __init__(self, msg):
        self.msg = msg

class KeyFiltersError(Error):
    '''  Raised when incoming parameter do not match with the 
    expected parameters by the engine. Engine parameters are
    setup under engine['PARAMETERS']['FILTER_PARAMETERS']
    -----
    '''
    pass

class IncomingParameterCombinationError(Error):
    ''' Raised when the incoming parameters do not match
    with engine['PARAMETERS']['PARAMETERS_COMBINATION']
    (if provided)
    -----
    '''
    pass

class IncomingParameterLengthError(Error):
    ''' Raised when the lenght of the incoming parameters
    do not match with the 'length' attribute of the configured
    parameters for the engine in engine['PARAMETERS']['FILTER_PARAMETERS']
    (if provided)
    -----
    '''
    def __init__(self, msg):
        self.msg = msg

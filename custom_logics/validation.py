from custom_logics import exceptions as ce
import json, sys

class ArgumentValidation(object):
    """ """
    def __init__(self, string_engine, incoming_filter_dict):
        with open("./config/dbengines.json") as config_file:
            self._engines_config = json.load(config_file)
        self.validation(self._engines_config, string_engine, incoming_filter_dict)

    def validation(self, engine_configuration, string_engine, incoming_filter_dict):
        self._val_res = {}
        self._val_res["ENGINE_NAME"] = string_engine
        self._val_res["ENGINE_ENVIRONMENT"] = self._validate_engine(string_engine)
        self._validate_incoming_filters(incoming_filter_dict)
        self._val_res["PARAMETER_COMBINATION"] = self._validate_incoming_filters_combination(incoming_filter_dict)
        self._val_res["INCOMING_PARAMETERS"] = self._validate_parameter_values(self._val_res["PARAMETER_COMBINATION"], incoming_filter_dict)
        return self._val_res

    def _validate_engine(self, string_engine):
        try:
            self._engines_config[string_engine]
            return self._engines_config[string_engine]
        except KeyError:
            raise ce.EngineError(string_engine)
        except Exception:
            raise Exception

    def _validate_incoming_filters(self, incoming_dict_filters):
        match_counter = 0
        for key in incoming_dict_filters["PARAMETERS"].keys():
            if key in self._val_res["ENGINE_ENVIRONMENT"]["PARAMETERS"]["FILTER_PARAMETERS"].keys():
                match_counter = match_counter + 1
        if match_counter == 0:
            raise ce.KeyFiltersError

    def _validate_incoming_filters_combination(self, incoming_dict_filters):
        try: 
            engine_parameters_combinations = list(self._val_res["ENGINE_ENVIRONMENT"]["PARAMETERS"]["PARAMETERS_COMBINATION"].values())
        except KeyError:
            return ''
        else:
            match_counter = 0
            comb = []
            combination_values = {}
            for combination in engine_parameters_combinations:
                #print(f"N° of matches: {set(combination) & set(incoming_dict_filters)} | N° of items in combination: {combination}{len(combination)}")
                if len(set(combination) & set(incoming_dict_filters["PARAMETERS"])) == len(combination):
                    match_counter = match_counter + 1
                    comb = combination
            if match_counter > 0:
                for item in comb:
                    combination_values[item] = self._val_res["ENGINE_ENVIRONMENT"]["PARAMETERS"]["FILTER_PARAMETERS"][item]["LENGTH"]
                return combination_values
            else:
                raise ce.IncomingParameterCombinationError
    
    def _validate_parameter_values(self, engine_parameter_settings, incoming_parameters):
        match_counter = 0
        match_error = {}
        if engine_parameter_settings == '':
            return incoming_parameters
        else:
            for key, value in engine_parameter_settings.items():
                if value == len(incoming_parameters["PARAMETERS"][key]):
                    match_counter = match_counter + 1
                else:
                    match_error[key] = incoming_parameters["PARAMETERS"][key]
            if match_counter == len(incoming_parameters["PARAMETERS"]):
                return incoming_parameters
            else:
                raise ce.IncomingParameterLengthError(match_error)

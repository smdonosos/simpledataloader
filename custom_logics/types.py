import argparse, ast, json

def _dic_type(string):
    try:
        dict = ast.literal_eval(string)
    except Exception:
        msg = f"\"{string}\" is not a valid dictionary argument."
        raise argparse.ArgumentTypeError(msg)
    return dict
from logger import LOG, LoggingHandler
import sys, abc, random

LoggingHandler()

class FieldMask:

    def __init__(self, mask_method):
        
        self._method = mask_method
        self._mask_functions = {
            'SampleName' : self._sample_name(),
            'SampleSurname' : self._sample_surname(),
            'SampleMiddleName' : self._sample_middle_name(),
            'RandomNumber' : self._random_number(),
            'base_m2_dob' : self._base_m2_dob(),
            'DateOfBirth' : self._dob(),
            'seg_dob' : self._seg_dob(),
            'FirstAddress' : self._first_address(),
            'SecondAddress' : self._second_address()
        }
    
    def return_masking_method(self):
        if self._is_valid_masking_method(self._method):
            return self._mask_functions[self._method]
        else:
            LOG.critical(f"Masking method '{self._method}' not found")
            sys.exit(-1)

    def _is_valid_masking_method(self, mask_method):
        if mask_method in self._mask_functions.keys():
            return True
        return False

    @staticmethod
    def _sample_name():
        names = ["Liam", "Noah", "Wiliam", "James", "Emma", "Olivia", "Ava", "Isabella"]
        return random.choice(names)

    @staticmethod
    def _sample_surname():
        surnames = ["Smith", "Johnson", "Williams", "Brown", "Jones", "Garcia", "Miller"]
        return random.choice(surnames)

    @staticmethod
    def _sample_middle_name():
        middle_names = ["Alexander", "Peter", "Edward", "May", "Claire", "Hope"]
        return random.choice(middle_names)

    @staticmethod
    def _random_number():
        return str(random.randrange(90000000, 99999998))

    @staticmethod
    def _dob():
        return '2019-01-01'
    
    @staticmethod
    def _base_m2_dob():
        return '+20190101'

    @staticmethod
    def _seg_dob():
        return '20190101'

    @staticmethod
    def _first_address():
        return '221 B Baker St'

    @staticmethod
    def _second_address():
        return ''
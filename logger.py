# -*- coding: utf-8 -*-

import json
import logging.config
import os

LOG = logging.getLogger(__name__)

class LoggingHandler():
    'Defines the logger to be used during the execution.'

    def __init__(self, *args, **kwargs):
        'Initializes logger with default values.'

        global LOG
        self._current_path = os.path.abspath(os.path.dirname(__file__))
        LOG = logging.getLogger(__name__)
        self.log_config_file = os.path.join(self._current_path,'config/logging_config.json')
        self.default_level = logging.INFO
        self.load_log_config()

    def load_log_config(self):
        'Loads log config from config file.'

        with open(self.log_config_file) as json_data:
            try:
                logging_config = json.load(json_data)
                logging.config.dictConfig(logging_config)
                LOG.debug('Log configuration correctly loaded.')
            except IOError as io_error:
                LOG.warn('Unable to load logging file. ' + str(io_error))
                LOG.warn('Loading basic configurations for logger...')
                logging.basicConfig(level=logging.DEBUG)

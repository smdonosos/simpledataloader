REM METRIC REPORT (DB2) SAMPLE COMMAND
REM ----------------------------------
REM python data_loader.py DB2-MR {'PARAMETERS':{'SUBMITTAL_ID_FILTER':'BT214DXU'}}
REM python data_loader.py DB2-MR {'PARAMETERS':{'SUBMITTAL_ID_FILTER':'BT215CAM'}}
REM python data_loader.py DB2-MR {'PARAMETERS':{'SUBMITTAL_ID_FILTER':'BT217QPT'}}
REM python data_loader.py DB2-MR {'PARAMETERS':{'SUBMITTAL_ID_FILTER':'BT218OWA'}}


REM ---------------------------------------
REM DISPUTE REPORT (VERTICA) SAMPLE COMMAND
REM ---------------------------------------
REM python data_loader.py DR {'PARAMETERS':{'subcode_filter':'1270246'}}
REM python data_loader.py DR {'PARAMETERS':{'subcode_filter':'1223850'}}
REM python data_loader.py DR {'PARAMETERS':{'subcode_filter':'1638640'}}

REM -----------------------------------
REM M2Reports (Vertica) Sample Commands
REM -----------------------------------
REM python data_loader.py M2F-TEST {'PARAMETERS':{'job_id_filter':'2988973'}}
REM python data_loader.py M2F-TEST {'PARAMETERS':{'job_id_filter':'2995645'}}


REM -----------------------------------------------------
REM Previous Submissions Report (Vertica) Sample Commands
REM -----------------------------------------------------
python data_loader.py PSR-STG {'PARAMETERS':{'job_id_filter':'3024217'}}
python data_loader.py PSR-STG {'PARAMETERS':{'job_id_filter':'2989539'}}

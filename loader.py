import os, re, argparse, pyodbc, json, sys, traceback, pandas as pd, abc, datetime
from custom_logics import types, exceptions as ce
from custom_logics import validation as val
from connection import DataBaseSource
from logger import LOG, LoggingHandler
from mask import FieldMask
from threading import Thread

LoggingHandler()

class Loader:

    def __init__(self, engine_validated):
        LOG.debug("Preparing environment for migration...")
        self._engine_name = engine_validated["ENGINE_NAME"]
        self._engine_conf = engine_validated["ENGINE_ENVIRONMENT"]
        self._incoming_parameters = engine_validated["INCOMING_PARAMETERS"]["PARAMETERS"]
        
        # Gathering information to be migrated
        self._retrieve_migratory_data()
        # Inserting information
        self._insert_migratory_data()
        
    def _retrieve_migratory_data(self):
        '''  '''
        environment = 'PRODUCTION'
        self._datasources = {}
        self._empty_dsrcs = []
        self._setting_db_vars(environment)
        
        # Checking additional optional parameters
        if self._check_additional_params():
            self._get_base_added_parameters(environment)
        else:
            self._get_base_parameters()
        
        # Building queries
        self._build_prod_datasource(self._engine_conf["CONNECTIONS"][environment]["CONNECTION_STRING"])
        # Retrieving data
        self._retrieve_prod_data()
        self._remove_empty_dsrcs()

    def _insert_migratory_data(self):
        '''  '''    
        environment = 'TEST'
        self._insert_dict = {}
        self._insert_mode = self._get_insert_mode()
        self._setting_db_vars(environment)

        # Building insert queries
        self._insert_connection = self._engine_conf["CONNECTIONS"][environment]["CONNECTION_STRING"]
        self._build_insert_datasources()
        self._run_inserts()

    def _setting_db_vars(self, environment):
        try:
            DataBaseSource.attemps = self._engine_conf["CONNECTIONS"][environment]["ATTEMPTS"]
            DataBaseSource.timeout = self._engine_conf["CONNECTIONS"][environment]["TIMEOUT"]
            DataBaseSource.delay = self._engine_conf["CONNECTIONS"][environment]["DELAY"]
        except:
            LOG.critical(traceback.print_exc())
            sys.exit(-1)

    def _get_base_added_parameters(self, environment):

        LOG.debug("Additional parameters=True, retrieving additional parameters...")
        self._datasources["PARAMETERS"] = DataBaseSource(
            self._engine_conf["CONNECTIONS"][environment]["CONNECTION_STRING"],
            self._get_added_params_qry()
        )._get_data_frame()
    
    def _get_base_parameters(self):
        LOG.debug("Additional parameters=False, building datasets with current parameters...")
        print(self._incoming_parameters)
        self._datasources["PARAMETERS"] = pd.DataFrame(
             [v for k, v in self._incoming_parameters.items()],
             columns=self._incoming_parameters.keys()
            )

    def _check_additional_params(self):
        """
        Will check if engine contains key ["CONNECTIONS"]["QUERY_FILE"]
        will return True if key found, and false if is empty or unexistant
        """
        try:
            query_file = self._engine_conf["PARAMETERS"]["QUERY_FILE"]
            if query_file.strip() == "":
                return False
            else:
                return True
        except KeyError:
            return False

    def _get_added_params_qry(self):
        ''' Retrieving additional data parameters 
        -----------------
        '''
        query = open(f"./db/{self._engine_name}/"\
                      f"{self._engine_conf['PARAMETERS']['QUERY_FILE']}","r").read()
        for key, value in self._incoming_parameters.items():
            query = query.replace(key, value)
        return query

    def _build_prod_datasource(self, connection):
        '''  '''
        for key, values in self._engine_conf["TABLES"].items():
            LOG.debug(f"Building dataset for table '{key}'")
            if 'QUERY_FILE' in values.keys():
                self._datasources[key] = DataBaseSource(
                    connection, self._build_qry_from_file(key, values)
                )
            else:
                self._datasources[key] = DataBaseSource(
                    connection, self._build_qry_from_values(key, values)
                )
    
    def _build_qry_from_file(self, table_tag, values):
        '''  '''
        try:
            path = f"./db/{self._engine_name}/{values['QUERY_FILE']}"
            qry = open(path, "r").read()
            if 'DF_FILTER' in values.keys():
                qry = self._replace_qry_filters(qry, values["DF_FILTER"])
                return qry
            else:
                qry = self._replace_qry_filters(qry)
                return qry
        except FileNotFoundError:
            LOG.warning(f"Query file: '{path}' not found for table: '{table_tag}'")
            return next
    
    def _replace_qry_filters(self, qry, opt_filter=None):
        """ Will match the special filter words in a query
            and will replace those for the incoming filters
        """
        df = self._datasources["PARAMETERS"]
        if opt_filter != None:
            df = df.loc[df[opt_filter["COLUMN"]] == opt_filter["VALUE"]]
            try:
                for column in df.columns.values:
                    qry = qry.replace(column, str(df[column].values[0]))
            except IndexError:
                LOG.critical(f"Filters: {opt_filter} are not vallid"\
                    " in the context used.")
                sys.exit(-1)
        else:
            for column in self._datasources["PARAMETERS"].columns.values:
                qry = qry.replace(column, str(df[column].values[0]))
        return qry
        
    def _build_qry_from_values(self, key, values):
        raise NotImplementedError
         
    def _gen_qry_header(self, key, env):
        raise NotImplementedError

    def _gen_qry_body(self, body_conf, env):
        raise NotImplementedError

    def _retrieve_prod_data(self):
        '''  '''
        for datasource_tag, datasource, in self._datasources.items():
            if datasource_tag != "PARAMETERS":
                try:
                    process = Thread(
                        target=self._load_datasource_data(datasource_tag, datasource)
                    )
                    process.start()
                except:
                    print(traceback.print_exc())
                    process._delete()
                    next
        
    def _load_datasource_data(self, datasource_tag, datasource):
        ''' Load data into the datasources dict {}, then check
        if datasource configuration has encrypted fields
        '''
        LOG.debug(f"Fetching data for table '{datasource_tag}'")
        self._datasources.update({datasource_tag: datasource._get_data_frame()})
        if not self._check_df_empty(datasource_tag):
            self._fields_encryption(datasource_tag)
        else:
            LOG.warning(f'No rows found for table "{datasource_tag}"')
            self._empty_dsrcs.append(datasource_tag)

    def _check_df_empty(self, datasource_tag):
        if self._datasources[datasource_tag].empty:
            return True
        return False

    def _fields_encryption(self, datasource_tag):
        ''' Check if Datasource configuration hast fields to mask
        and trigger the _do_encryption function.
        --------------
        '''
        table_conf = self._engine_conf["TABLES"][datasource_tag]
        try:
            for column, mask_method in table_conf['ENCRYPT_FIELDS'].items():
                self._datasources.update(
                    {
                        datasource_tag : self._do_encryption(datasource_tag, column
                                                             ,mask_method
                                                             )
                    }
                )
        except KeyError:
            LOG.warning(f"Table '{datasource_tag}' doesn't contain encryption configuration")
            pass

    def _do_encryption(self, datasource_tag, datasource_column, masking_method):
        ''' Encrypt the column in the datasrouce using the encryption
        method detailed in the engine configuration for that Table/Column
        --------------------
        '''
        LOG.warning(
            f"Encrypting '{datasource_tag}.{datasource_column}', method='{masking_method}'"
        )
        df = self._datasources[datasource_tag]
        df.ix[0:len(df.index), datasource_column] = FieldMask(masking_method).return_masking_method()
        return df

    def _remove_empty_dsrcs(self):
        if len(self._empty_dsrcs) > 0:
            for dsource in self._empty_dsrcs:
                del self._datasources[dsource]
    
    def _get_insert_mode(self):
        ''' Insert mode available only for Vertica '''
        try:
            mode = self._engine_conf["ENGINE"]
            return mode
        except KeyError:
            return 'Default'
    
    def _build_insert_datasources(self):
        LOG.debug("Building insert sources...")
        
        if self._insert_mode == 'VERTICA':
            self._build_vertica_datasource()    
        else:
            self._build_default_datasource()

    def _build_vertica_datasource(self):
        ''' Building Vertica Insert method
        ----------
        '''
        for datasource_tag in self._datasources.keys():
            if datasource_tag != 'PARAMETERS':
                self._datasource_to_disk(datasource_tag)
                self._build_vertica_copy(datasource_tag)
    
    def _datasource_to_disk(self, datasource_tag):
        ''' Store datasource (pandas dataframe) to disk '''
        LOG.debug(f'Writing file "{datasource_tag}.gzip..."')
        path = os.path.join(
            os.path.dirname(__file__),
            'db',
            self._engine_name,
            'temp',
            datasource_tag+'.gzip'
        )
        self._datasources[datasource_tag].to_csv(path, index=False, compression='gzip')

    def _build_vertica_copy(self, datasource_tag):
        ''' Method to load data to vertica DB using COPY [] FROM LOCAL 
        command: This require write permisions to table and USE permissions
        over involved schemas (contact your DB)
        ------
        '''
        path = os.path.join(
            os.path.dirname(__file__),
            'db',
            self._engine_name,
            'temp',
            datasource_tag+'.gzip'
        )
        table = f"{self._engine_conf['SCHEMA']['TEST']}.{datasource_tag}"
        qry_path = f'./db/{self._engine_name}/vertica_copy.sql'
        copy_qry = open(qry_path, 'r').read().replace('table',table).replace('file_name',path)
        self._insert_dict[datasource_tag] = DataBaseSource(self._insert_connection, copy_qry)
    
    def _build_default_datasource(self):
        ''' Default insert statement method.
        Will create insert statements for each row/dataset
        on dict self._datasources
        -----------------------
        '''
        schema = self._engine_conf["SCHEMA"]["TEST"]
        for datasource_tag in self._datasources.keys():
            # Excluding empty datasets and "PARAMETERS" dataset
            if datasource_tag == "PARAMETERS":
                next
            else:
                insert_items = []
                for row in self._datasources[datasource_tag].itertuples(index=False):
                    values = []
                    for item in row:
                        if type(item) == datetime.date:
                            values.append(str(item))
                        else:
                            values.append(item)
                    insert_items.append(values)
                #
                # Aditional Parameters for insert statement
                opt_params ={
                    "TABLE": datasource_tag,
                    "SCHEMA": schema,
                    "COLUMNS":str(list(self._datasources[datasource_tag].columns.values)).replace('[','').replace(']','').replace("'",""),
                    "COLVALS":str(['?' for column in self._datasources[datasource_tag].columns.values]).replace('[','').replace(']','').replace("'","")
                }
                #
                # Inserting Datasource info to dict {}
                self._insert_dict[datasource_tag] = DataBaseSource(self._insert_connection, insert_items, opt_params)

    def _run_inserts(self):
        '''  '''
        if self._insert_mode == 'VERTICA':
            for datasource_tag, datasource in self._insert_dict.items():
                try:
                    LOG.debug(f"Running vertica copy over table '{datasource_tag}'...")
                    datasource._run_vertica_copy()
                    LOG.debug(f"Copy insert completed for '{datasource_tag}'!")
                except:
                    traceback.print_exc()
                    return next
        else:
            for datasource_tag, datasource in self._insert_dict.items():
                try:
                    LOG.debug(f"Attempting to insert data to table '{datasource_tag}'")
                    datasource._run_sql_statement()
                    LOG.debug(f"Data insert for '{datasource_tag}' completed")
                except:
                    traceback.print_exc()
                    return next
            